---
title: M999 - jeu d'instruction
---

| op0   | op1 op2   | mnémonique | instruction à réaliser                                                              |
|-------|-----------|------------|-------------------------------------------------------------------------------------|
| 0     | _addr_    | `LDA`      | copie le mot mémoire d’adresse _addr_ dans le registre A                            |
| 1     | _addr_    | `LDB`      | copie le mot mémoire d’adresse _addr_ dans le registre B                            |
| 2     | _addr_    | `STR`      | copie le contenu du registre R dans le mot mémoire d'adresse _addr_                 |
| **3** |           |            | **opérations arithmétiques et logiques**                                            |
| 3     | 0 0       | `ADD`      | ajoute les valeurs des registres A et B, produit le résultat dans R                 |
| 3     | 0 1       | `SUB`      | soustrait la valeur du registre B à celle du registre A, produit le résultat dans R |
| 3     | . .       | etc        | …                                                                                   |
| 3     | 9 9       | `NOP`      | ne fait rien                                                                        |
| 4     | _rs_ _rd_ | `MOV`      | copie la valeur du registre source _rs_ dans le registre destination _rd_           |
| 5     | _addr_    | `JMP`      | branche en _addr_ (PC reçoit la valeur _addr_)                                      |
| 6     | _addr_    | `JPP`      | branche en _addr_ si la valeur du registre R est strictement positive               |

Les registres sont désignés par les valeurs suivantes :

valeur | registre
------ | --------
0 | A
1 | B
2 | R

### Boot et arrêt

La machine démarre avec la valeur nulle comme pointeur d'instruction.

La machine stoppe si le pointeur d'instruction vaut 99.

On peut donc utiliser le mnémonique `HLT` comme synonyme de `JMP 99`.

### Entrées/sorties

Les entrées/sorties sont "mappées" en mémoire.

Écrire le mot mémoire 99 écrit sur le terminal.

Les valeurs saisies sur le terminal seront lues dans le mot mémoire 99.

## Gestion de la pile 

op0 | op1 op2 | mnémonique | instruction à réaliser
--- | ------- | ---------- | ----------------------
7   | _addr_  | `CAL`      | copie la valeur de PC dans la case mémoire SP ; décrémente SP ; copie _addr_ dans PC
3   | 9 8     | `RET`      | incrémente SP ; copie la valeur de la case mémoire SP dans PC
3   | 9 7     | `PSH`      | incrémente SP ; copie la valeur de R dans la case mémoire SP 
3   | 9 6     | `POP`      | copie la valeur de la case mémoire SP dans R ; décrémente SP 
3   | 8 _d_   | `SSP`      | copie la valeur de R dans la case mémoire SP+_d_
3   | 7 _d_   | `GSP`      | copie la valeur de la case mémoire SP+_d_ dans R 

Le registre SP contient la valeur 98 au démarrage de la machine. 

## Extension registre

Les registres sont désignés par les valeurs suivantes :

| valeur | registre source | registre destination |
|--------|-----------------|----------------------|
| 0      | A               | A                    |
| 1      | B               | B                    |
| 2      | R               | R                    |
| 3      | R3              | R3                   |
| 4      | R4              | R4                   |
| 5      | R5              | R5                   |
| 6      |                 | échange avec R       |
| 7      |                 | échange avec R3      |
| 8      | valeur 0        | échange avec R4      |
| 9      | valeur 1        | échange avec R5      |

